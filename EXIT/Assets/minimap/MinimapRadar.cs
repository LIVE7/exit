﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MinimapRadar : MonoBehaviour
{

    public Image image;
    
    // Use this for initialization
    void Start()
    {
        minimap.RegisterRadarObject(this.gameObject, image);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDestroy()
    {
        minimap.RemoveRadarObject(this.gameObject);
    }

    public void deleteIcon()
    {
        minimap.RemoveRadarObject(this.gameObject);
    }
}
